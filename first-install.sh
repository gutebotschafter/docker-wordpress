#!/usr/bin/env bash

echo "Removes old git..."
rm -rf .git
echo ""

echo "Initialize Git..."
echo ""
git init
echo ""
echo "Enter your remote repository"
echo "Ex: git@bitbucket.org:gutebotschafter/repository.git"
echo ""
read head
git remote add origin ${head}
git add .
git branch -m production
git push origin production
git checkout -b staging
git push origin staging
echo ""

echo "Adding gb-base theme as sub repository"
git submodule add git@bitbucket.org:gutebotschafter/gb-base.git wp-themes/gb-base
echo ""

echo "Adding gb-blocks as sub repository"
git submodule add git@bitbucket.org:gutebotschafter/gb-blocks.git wp-blocks/gb-blocks
echo ""

docker-compose up -d

cp initial-setup/.env.local .env

chmod +x install-wordress.sh
./install-wordpress.sh

echo "Starting Post-Install Script"
echo ""
./post-install.sh
echo ""

echo "Installing Updater Endpoint"
echo ""
./setup-updater.sh
echo ""

echo "Installing Activator"
echo ""
./setup-updater.sh

chmod +x create-symlinks.sh
./create-symlinks.sh

rm -f first-install.sh
