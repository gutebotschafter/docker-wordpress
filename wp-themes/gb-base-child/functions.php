<?php

function pwaStyles() {
	echo "<meta name=\"theme-color\" content=\"#ff671f\" />";
}

function deregister_script() {
	wp_deregister_script("wp-embed");
}

function deregister_style() {
	wp_deregister_style( 'dashicons' );
}

function gutenberg_styles() {
	wp_enqueue_style( 'gutenberg-css', get_theme_file_uri( '/gutenberg.css' ), false );
}

add_action('wp_head', 'pwaStyles');
add_action("wp_enqueue_scripts", "deregister_script", 9999);
add_action("wp_print_styles", "deregister_style");
add_filter("emoji_svg_url", "__return_false");
add_filter('show_admin_bar', '__return_false');
add_action( 'enqueue_block_editor_assets', 'gutenberg_styles' );

remove_action("wp_print_styles", "print_emoji_styles");

remove_action("wp_head", "print_emoji_detection_script", 7);
remove_action('wp_head', 'rest_output_link_wp_head');
remove_action('wp_head', 'wp_resource_hints', 2);
remove_action('wp_head', 'wp_oembed_add_discovery_links');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
