import { isMobileOpened } from './navigation-mobile';

let $activeDropdown = null;
let $dropdowns = null;
let $header = null;

/**
 * Hides the navigation overlay
 *
 * @returns {void}
 */
const removeActiveSubNavigation = () => {
    const $selected =
        $header && $header.querySelectorAll('.menu-item.sub-menu--show');

    Array.prototype.forEach.call($selected, $item =>
        $item.classList.remove('sub-menu--show')
    );

    $activeDropdown = null;
};

/**
 * Returns top position from header
 *
 * @returns {string}
 */
const getBoundingClientRect = () =>
    `${$header.getBoundingClientRect().height -
        parseFloat(
            window
                .getComputedStyle($header, null)
                .getPropertyValue('padding-bottom')
        )}px`;

/**
 * Handles hover and click on navigation
 *
 * @param $item
 * @returns {void}
 */
const handleActivation = $item => {
    const $dropdown = $item.querySelector('.sub-menu');

    $item.addEventListener('mouseenter', () => {
        if (isMobileOpened()) {
            return;
        }

        removeActiveSubNavigation();

        if ($dropdown) {
            $dropdown.style.top = getBoundingClientRect();
        }

        $item.classList.add('sub-menu--show');
        $activeDropdown = $item;
    });

    $header.addEventListener('mouseleave', () => removeActiveSubNavigation());
};

/**
 * Updates top position on scroll event
 *
 * @returns {void}
 */
const handleScrollEvent = () => {
    if ($activeDropdown) {
        $activeDropdown.style.top = getBoundingClientRect();
    }
};

/**
 * Initiliaze the dropdown navigation
 *
 * @returns {void}
 */
export const init = () => {
    $header = document.querySelector('.page-header');
    $dropdowns = $header && $header.querySelectorAll('.menu-item-has-children');

    // const $menuItems =
    //     $header &&
    //     $header.querySelectorAll('.menu-item:not(.menu-item-has-children)');
    //
    // if ($menuItems) {
    //     Array.prototype.forEach.call($menuItems, $item =>
    //         $item.addEventListener('mouseenter', () =>
    //             removeActiveSubNavigation()
    //         )
    //     );
    // }

    if ($dropdowns) {
        Array.prototype.forEach.call($dropdowns, $item =>
            handleActivation($item)
        );

        // document.addEventListener("scroll", () => handleScrollEvent());
    }
};
