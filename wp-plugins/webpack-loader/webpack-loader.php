<?php

defined('ABSPATH') or die("Thanks for visting");

/**
 * Plugin Name:  webpack loader plugin
 * Plugin URI: https://gute-botschafter.de
 * Description: This plugin hooks files from webpack into header and footer
 * Version: 1.0.0
 * Author: Dennis Stockhofe
 * Author URI: https://gute-botschafter.de
 * License: GPL2
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:  webpack
 */

define('DS', DIRECTORY_SEPARATOR);

/**
 * Checks if the file exists and put the content into the returning string
 *
 * @param string $file The file to get the inline content
 * @param string $tag The tag for inline content
 * @return void|string Returns the string if there is content or false if there is nothing
 */
function criticalRenderer($file, $tag = "style")
{
	$path = __DIR__ . "/../../public" . $file;

    if (file_exists($path)) {
        $content = file_get_contents($path);

        return "<" . $tag . ">" . $content . "</" . $tag . ">";
    }

    return false;
}

/**
 * Create critical inline styles
 */
function hookCriticalStyles()
{
    echo criticalRenderer("/assets/criticals.bundle.css");
}

/**
 * Adds webpack styles into header
 */
function hookStyles()
{
    echo "<link rel=\"stylesheet\" href=\"/assets/app.bundle.css\">";
}

/**
 * Adds webpack javascript into footer
 */
function hookJavascript()
{
    echo "<script src=\"/assets/app.bundle.js\" deferr></script>";
}

add_action('wp_head', 'hookCriticalStyles');
add_action('wp_head', 'hookStyles');
add_action('wp_footer', 'hookJavascript');
