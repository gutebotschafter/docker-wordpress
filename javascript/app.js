import './polyfills';
import './service-worker';
import './assets-loader';

import * as navigation from './components/navigation';
import * as mobileNavigation from './components/navigation-mobile';

navigation.init();
mobileNavigation.init();
