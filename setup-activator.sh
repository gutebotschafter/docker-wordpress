#!/usr/bin/env bash

echo "Installing updater endpoint"
echo ""

echo "Deleting old files"
rm -rf activator.phar
rm -rf activator.sh

echo ""
git clone git@bitbucket.org:gutebotschafter/wordpress-activator-phar-cli.git wp-activator

cd wp-activator/app
php composer.phar install
cd ..

echo ""
php create-phar.php
mv activator.phar ../activator.phar

chmod +x activator.sh
mv activator.sh ../activator.sh

ACTIVATOR=../activator.yml
if [[ -f "$ACTIVATOR" ]]; then
    echo
    echo "activator.yml exists... skipping"
else
    mv activator.yml ../activator.yml
fi

cd ..
rm -rf wp-activator

echo ""
echo "activator installed..."
