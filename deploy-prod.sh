#!/usr/bin/env bash

SOURCES="./" # LOCAL SYSTEM
PORT=22 # SSH PORT ON PRODUCTION SYSTEM
TARGET="" # PRODUCTION SYSTEM => ex.: user@host:/path

RSYNCCONF="-rtpgo"

rsync ${RSYNCCONF} --progress -e "ssh -p $PORT -o 'StrictHostKeyChecking=no'" ${SOURCES} ${TARGET}

exit 0
