#!/usr/bin/env bash

if [[ $1 == "" ]]; then
    echo "Missing Container Parameter! Usage: ./docker-connect.sh [container]"
    echo "ex: ./docker-connect.sh web"
    echo
    echo "Available Container:"
    docker ps --format "{{.Names}}" | cut -d'_' -f 2
    exit
fi

DOCKERID=$(docker ps | grep _$1_ | cut -d' ' -f1)

docker exec -it "$DOCKERID" /bin/bash
