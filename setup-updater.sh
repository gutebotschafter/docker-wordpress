#!/usr/bin/env bash

echo "Installing updater endpoint"
echo

echo "Deleting old files"
rm -rf public/updater
rm -rf updater-endpoint
rm -f create-access-token.sh
rm -f updater.phar
rm -f updater.sh

echo
git clone git@bitbucket.org:gutebotschafter/updater-endpoint.git updater-endpoint-temp
mv updater-endpoint-temp/public/updater web/updater
mv updater-endpoint-temp/updater-endpoint updater-endpoint/
mv updater-endpoint-temp/create-access-token.sh create-access-token.sh

SYNC=./sync-files.sh
if [[ -f "$SYNC" ]]; then
    echo "Sync files script found... skipping"
else
    mv updater-endpoint-temp/sync-files.sh sync-files.sh
fi

rm -rf updater-endpoint-temp
echo

TOKEN=./updater-token.php
if [[ -f "$TOKEN" ]]; then
    echo "Access token found... skipping"
    echo
else
    echo "Generating access token"
    sh -c ./create-access-token.sh
    echo
fi

git clone git@bitbucket.org:gutebotschafter/updater-phar.git

cd updater-phar/app
php composer.phar install

cd ..
php create-phar.php
mv updater.phar ../updater.phar
mv updater.sh ../updater.sh
cd ..

UPDATER=./updater.yml
if [[ -f "$UPDATER" ]]; then
    echo
    echo "UPDATER exists... skipping"
else
    mv updater-phar/updater-default.yml updater.yml
fi

rm -rf updater-phar

echo
echo "updater installed..."
