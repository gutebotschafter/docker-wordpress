#!/usr/bin/env bash

echo "Linking themes into wordpress..."
cd public/wp-content/
rm -rf themes
ln -s ../../wp-themes/ themes
cd ../..
echo ""

echo "Linking blocks into wordpress..."
cd ./public/wp-content/plugins/
rm -rf gb-blocks
ln -s ../../../wp-blocks/gb-blocks gb-blocks
echo ""
echo "Linking plugins into wordpress"
rm -rf webpack-loader
ln -s ../../../wp-plugins/webpack-loader webpack-loader
cd ../../..
echo ""

echo "Linking uploads into wordpress..."
cd ./public/wp-content
rm -rf uploads
ln -s ../../wp-uploads uploads
echo ""
