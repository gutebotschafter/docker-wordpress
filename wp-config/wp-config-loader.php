<?php

$env = file_get_contents(__DIR__ . "/../.env");
$wpConfig =  __DIR__ . "/wp-config-" . trim($env) . ".php";

if (file_exists($wpConfig)) {
	/** @noinspection PhpIncludeInspection */
	require_once $wpConfig;
}

define( 'DISALLOW_FILE_EDIT', true );
define( 'AUTOMATIC_UPDATER_DISABLED', true );
