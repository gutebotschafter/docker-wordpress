#!/usr/bin/env bash

file="https://de.wordpress.org/latest-de_DE.tar.gz"

echo "Downloading latest Wordpress Version..."
echo ""
curl -O ${file}
echo ""

echo "Deletes the public folder..."
rm -rf ./public
echo ""

echo "Unzips Wordpress to Public..."
tar -xzf latest-de_DE.tar.gz -C ./
mv wordpress public
touch public/.gitkeep
echo ""

echo "Adds new .htaccess..."
cp ./initial-setup/.htaccess ./public/.htaccess
cp ./initial-setup/.htaccess.subfolder ./public/wp-content/uploads.htaccess
cp ./initial-setup/.htaccess.subfolder ./public/wp-includes/.htaccess
echo ""

echo "Adds new wp-config..."
cp ./initial-setup/wp-config.php ./public/wp-config.php
echo ""

echo "Downloading newest wp-cli..."
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
echo ""

echo "Copying updater to web-root..."
cp -R ./initial-setup/updater/ ./public/updater
echo ""

echo "Cleans up..."
rm -f ./public/license.txt
rm -f ./public/liesmich.html
rm -f ./public/readme.html
rm -f ./public/wp-config-sample.php
rm -f ./latest-de_DE.tar.gz
echo
