# wordpress with docker and webpack-dev-server

## Table of Contents

  - [Installation](#installation)
  - [Usage](#usage)
    - [Installation](#installation)
    - [Docker](#docker)
    - [Container](#container)
    - [Connect](#connect)
    - [npm](#npm)
    - [webpack](#webpack)
    - [Database](#database)
  - [Deployment](#deployment)
  - [Update](#update)
  - [Debugging with xdebug](#debugging-with-xdebug)
  - [Codestyling](#codestyling)
    - [PHP](#php)
    - [Javascript](#javascript)
  - [Local tools](#local-tools)

## Installation

Please make sure, that you installed the [local tools](#local-tools).

Run `docker-compose build` to pulling the image and building the container.

## Usage

First - Open the terminal and change to the project directory.

### Installation

Start with the `./first-install.sh`-Script to install the latest Wordpress Version with Standard-Configuration.

**Attention**: The install-Script will delete existing data!

### Docker

To start the docker-container run the command `docker-compose up`. (Shows log output)
If you want to start the container in the Background use `docker-compose up -d`.

### Container
To list the container use `docker ps`, you need the web_1 container name. Example:

| CONTAINER ID  | IMAGE        | COMMAND                | CREATED    | STATUS        | PORTS                  | NAMES                            |
| ------------- | ------------ | ---------------------- | -----------| --------------| -----------------------| ---------------------------------|
| e2a8bb8ed4ae  | nginx:latest | "nginx -g 'daemon of…" | 1 days ago | Up 34 minutes | 0.0.0.0:80->80/tcp,... | wordpress-docker-webpack_web_1   |
| 11f806bf2c51  | php:7-fpm    | "docker-php-entrypoi…" | 1 days ago | Up 34 minutes | 9000/tcp               | wordpress-docker-webpack_php_1   |
| 41c84d28ba5b  | phpmyadmin   | "/run.sh supervisord…" | 1 days ago | Up 34 minutes | 0.0.0.0:81->80/tcp     | wordpress-docker-webpack_pma_1   |
| fcf742d6b6d8  | mysql:5.7    | "docker-entrypoint.s…" | 1 days ago | Up 34 minute  | 0.0.0.0:3306->3306/tcp | wordpress-docker-webpack_mysql_1 |

### Connect

To connect to the container, you can use the script `./docker-connect.sh [container]`.

Replace the `[container]` with the Container Names, like `web`, `php` and so on...

### npm

Execute `./docker-connect.sh web` to connect to the web Container.

Switch to the `/var/www/html` directory and run `npm i` to install all node modules.

### webpack

Execute the command `./webpack-dev-server.sh` to start the webpack-dev-server. The javascript entry point can be found in /src/app.js

**Attention:** Please activate the `webpack loader plugin` Plugin in the Wordpress Backend.

### Database

Also a phpMyAdmin is installed and accessible over http:/localhost:81

User/Password: `wordpress/wordpress`

Also see the database configuration for all environments in `./wp-config` folder

## Deployment

If the production server doesn't have npm installed, remove the assets directory `/public/assets` from the gitignore and commit after you have run `npm run build` (for Production the assets).

## Update

Update node modules only over npm!

## Debugging with xdebug

You can debug over xdebug directly into the container. In order to debug the php on the container, you must setup a new local ip like
```
sudo ifconfig en0 alias 10.254.254.254 255.255.255.0
```
Now you can debug with xdebug on port **9001** with the ip **10.254.254.254**. This ist important for the container to share xdebug data to the host machine.

## Codestyling

### php

For php codestyling follow the [PSR/2 Standard](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md)

### Javascript

This package includes an ESLint config (**.eslintrc**, from [Airbnb](https://github.com/airbnb/javascript)) and a prettier config (**.prettierrc**), to format the javascript code.

Activate the Plugins in your favourite IDE.

## Local tools

Make sure, that you have installed the following tools:

- docker ce (https://hub.docker.com/editions/community/docker-ce-desktop-mac)
