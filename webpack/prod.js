const webpack = require('webpack');
const { merge } = require('webpack-merge');
const { join, resolve } = require('path');
const common = require('./common');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = merge(common, {
    entry: {
        app: [
            join(__dirname, '../javascript/app'),
            resolve(__dirname, '../assets/styles/general.scss')
        ],
        criticals: resolve(__dirname, '../assets/styles/criticals.scss'),
    },
    mode: 'production',
    devtool: 'hidden-source-map',
    plugins: [
        new TerserPlugin({
            sourceMap: true,
            parallel: true
        }),
        new webpack.optimize.ModuleConcatenationPlugin(),
    ]
});
