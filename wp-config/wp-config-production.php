<?php
/**
 * Grundeinstellungen für WordPress
 *
 * Zu diesen Einstellungen gehören:
 *
 * * MySQL-Zugangsdaten,
 * * Tabellenpräfix,
 * * Sicherheitsschlüssel
 * * und ABSPATH.
 *
 * Mehr Informationen zur wp-config.php gibt es auf der
 * {@link https://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}
 * Seite im Codex. Die Zugangsdaten für die MySQL-Datenbank
 * bekommst du von deinem Webhoster.
 *
 * Diese Datei wird zur Erstellung der wp-config.php verwendet.
 * Du musst aber dafür nicht das Installationsskript verwenden.
 * Stattdessen kannst du auch diese Datei als wp-config.php mit
 * deinen Zugangsdaten für die Datenbank abspeichern.
 *
 * @package WordPress
 */

// ** MySQL-Einstellungen ** //
/**   Diese Zugangsdaten bekommst du von deinem Webhoster. **/

/**
 * Ersetze datenbankname_hier_einfuegen
 * mit dem Namen der Datenbank, die du verwenden möchtest.
 */
define( 'DB_NAME', '' );

/**
 * Ersetze benutzername_hier_einfuegen
 * mit deinem MySQL-Datenbank-Benutzernamen.
 */
define( 'DB_USER', '' );

/**
 * Ersetze passwort_hier_einfuegen mit deinem MySQL-Passwort.
 */
define( 'DB_PASSWORD', '' );

/**
 * Ersetze localhost mit der MySQL-Serveradresse.
 */
define( 'DB_HOST', '' );

/**
 * Der Datenbankzeichensatz, der beim Erstellen der
 * Datenbanktabellen verwendet werden soll
 */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Der Collate-Type sollte nicht geändert werden.
 */
define('DB_COLLATE', '');

/**#@+
 * Sicherheitsschlüssel
 *
 * Ändere jeden untenstehenden Platzhaltertext in eine beliebige,
 * möglichst einmalig genutzte Zeichenkette.
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * kannst du dir alle Schlüssel generieren lassen.
 * Du kannst die Schlüssel jederzeit wieder ändern, alle angemeldeten
 * Benutzer müssen sich danach erneut anmelden.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '=`rHa<CF,eM?o=55[u5UR{TM;QBN01pl@108Y|i(*P>4l73r<)Yn(aVR5Bw$eE2s' );
define( 'SECURE_AUTH_KEY',  'Ooj(]=!x0G}j~c_9&G~b;XnT[qQ5GNO~(>}e~uO-Q>x+_@^2!j%=}_xZuc2T8+@r' );
define( 'LOGGED_IN_KEY',    'z64/1e+E(&lpa^_-(5@Z?28{MU9E;uNxR%Ik.XSlKPP,[dStMBu_U9C]ji{AvN,r' );
define( 'NONCE_KEY',        '&gj,?bT@9R0E_SZT:=6;?Vx,_A#kA2_K^c7NF%nMVs>}xP}BI$6;be; I(s2|p,/' );
define( 'AUTH_SALT',        'iJmQFr95.4,oTWVFv3uU+_K~M.i;QlmBOi147oglQ6ls_9//{]31vb_wtNZ@=Z:^' );
define( 'SECURE_AUTH_SALT', '8a2]R<ex8w6d!)|.oOh1yRnmk?`@bxept5Emu5,2&1B<F%)Ncsu$2_i0sV42QB.K' );
define( 'LOGGED_IN_SALT',   ',N*EZru+|wC|xae5?`%]G`M/^!aKvp_1<Ii8fQdDQb~,.Wh|IXDKSpcGCF6%P6t]' );
define( 'NONCE_SALT',       ';RBm7?>GIEB{/^4D=GXC[<du}49WPIb-b.?E;k:B(XzANaJJs1^2~J_/c,,v|wWn' );

/**#@-*/

/**
 * WordPress Datenbanktabellen-Präfix
 *
 * Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank
 * verschiedene WordPress-Installationen betreiben.
 * Bitte verwende nur Zahlen, Buchstaben und Unterstriche!
 */
$table_prefix = 'wp_';

/**
 * Für Entwickler: Der WordPress-Debug-Modus.
 *
 * Setze den Wert auf „true“, um bei der Entwicklung Warnungen und Fehler-Meldungen angezeigt zu bekommen.
 * Plugin- und Theme-Entwicklern wird nachdrücklich empfohlen, WP_DEBUG
 * in ihrer Entwicklungsumgebung zu verwenden.
 *
 * Besuche den Codex, um mehr Informationen über andere Konstanten zu finden,
 * die zum Debuggen genutzt werden können.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

define( 'WP_POST_REVISIONS', 2 );
